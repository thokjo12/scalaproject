import exceptions._

class Account(val bank: Bank, initialBalance: Double) {

  val balance = new Balance(initialBalance)
  val uid :Int = bank.generateAccountId

  def withdraw(amount: Double): Unit = {
    amount match {
      case b if b > balance.amount =>
        throw new NoSufficientFundsException("insufficient balance")
      case x if x < 0 =>
        throw new IllegalAmountException("cant withdraw negative numbers")
      case `amount` => balance.synchronized {
        balance.amount -= amount
        true
      }
    }
  }

  def deposit(amount: Double): Unit = {
    amount match {
      case x if x < 0 =>
        throw new IllegalAmountException("cant deposit negative numbers")
      case `amount` => balance.synchronized {
        balance.amount += amount
      }
    }
  }

  def hasSufficientFunds(requestedSum: Double): Boolean = {
    balance.amount >= requestedSum
  }

  def getBalanceAmount: Double = {
    balance.amount
  }

  def transferTo(account: Account, amount: Double): Unit = {
    bank.addTransactionToQueue(this, account, amount)

  }

  class Balance(var amount: Double) {}


}
