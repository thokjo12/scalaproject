import exceptions._
import scala.collection.mutable

class TransactionQueue {
  private var queue: mutable.Queue[Transaction] = mutable.Queue()

  // Remove and return the first element from the queue
  def pop: Transaction = {
    queue.synchronized {
      queue.dequeue()
    }
  }

  // Return whether the queue is empty
  def isEmpty: Boolean = {
    queue.isEmpty
  }

  // Add new element to the back of the queue
  def push(t: Transaction): Unit = {
    queue.synchronized {
      queue.enqueue(t)
    }
  }

  // Return the first element from the queue without removing it
  def peek: Transaction = {
    queue.front
  }

  def size: Int = {
    queue.size
  }

  // Return an iterator to allow you to iterate over the queue
  def iterator: Iterator[Transaction] = {
    queue.iterator
  }
}

class Transaction(val transactionsQueue: TransactionQueue,
                  val processedTransactions: TransactionQueue,
                  val from: Account,
                  val to: Account,
                  val amount: Double,
                  val allowedAttemps: Int) extends Runnable {

  var status: TransactionStatus.Value = TransactionStatus.PENDING

  override def run(): Unit = {

    if (from.uid < to.uid) {
      from.synchronized {
        to.synchronized {
          doTransaction()
        }
      }
    } else {
      to.synchronized {
        from.synchronized {
          doTransaction()
        }
      }
    }

    //look into using a future to handle possible retry's, if some transaction contains a to target we can then
    // fire off the future and run it, it would also have a timer that prevents a locked transaction so if some transaction
    // waits for more than 100 ms it fails and the future returns a failed attempt.
    // Extend this method to satisfy requirements.
    def doTransaction(): Unit = {
      var attempts: Int = 0

      if (amount < 0) {
        status = TransactionStatus.FAILED
        processedTransactions.push(this)
        return
      }

      while (true) {
        if (attempts == allowedAttemps) {
          status = TransactionStatus.FAILED
          processedTransactions.push(this)
          return
        } else if (from.hasSufficientFunds(amount)) {
          from.withdraw(amount)
          to.deposit(amount)
          status = TransactionStatus.SUCCESS
          processedTransactions.push(this)
          return
        }
        attempts += 1
        Thread.sleep(10)
      }
    }
  }
}

object TransactionStatus extends Enumeration {
  val SUCCESS, PENDING, FAILED = Value
}
