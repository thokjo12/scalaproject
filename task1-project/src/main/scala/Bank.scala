//scala version is deprecated, should use the java version as the deprecation message states.
import java.util.concurrent.ForkJoinPool
import java.util.UUID.randomUUID
import java.util.concurrent.atomic.AtomicInteger

import scala.concurrent.ExecutionContext

class Bank(val allowedAttempts: Integer = 3) {
  private val uid = randomUUID()
  private val transactionsQueue: TransactionQueue = new TransactionQueue()
  private val processedTransactions: TransactionQueue = new TransactionQueue()
  private val executorContext = ExecutionContext.fromExecutorService(new ForkJoinPool(20))
  private val counter: AtomicInteger = new AtomicInteger(0)

  def addTransactionToQueue(from: Account, to: Account, amount: Double): Unit = {
    transactionsQueue.push(new Transaction(transactionsQueue, processedTransactions, from, to, amount, allowedAttempts))
    processTransaction()
  }

  /**
    */
  private def processTransaction(): Unit = {
    executorContext.submit(transactionsQueue.pop)
  }

  /**
    * several ways to do this one, one would just be to use the atomicIntegers function getAndIncrement
    * The other would be to recursively extract the Countervalue, into a old val, and a newval which has been incremented
    * then use the compareAndSet function, if that fails, recursively call the function until it passes.
    *
    * @return a new id
    */
  // Hint: use a counter
  def generateAccountId: Int = {
    counter.getAndIncrement()
  }

  def addAccount(initialBalance: Double): Account = {
    new Account(this, initialBalance)
  }

  def getProcessedTransactionsAsList: List[Transaction] = {
    processedTransactions.iterator.toList
  }

}
